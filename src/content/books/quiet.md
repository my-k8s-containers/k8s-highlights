---
type: books
weight: 6
title: Quiet
subtitle: The Power of Introverts in a World That Can't Stop Talking
author: Susan Cain
thumbnail: images/quiet.jpg
---

Nor are introverts necessarily shy. Shyness is the fear of social
disapproval or humiliation, while introversion is a preference for
environments that are not overstimulating. Shyness is inherently
painful; introversion is not

As Jung felicitously put it, “There is no such thing as a pure extrovert
or a pure introvert. Such a man would be in the lunatic asylum.”

The word personality didn’t exist in English until the eighteenth
century, and the idea of “having a good personality” was not widespread
until the twentieth. But when they embraced the Culture of Personality,
Americans started to focus on how others perceived them.@ 2019-12-10

Adler explained that all infants and small children feel inferior,
living as they do in a world of adults and older siblings. In the normal
process of growing up they learn to direct these feelings into pursuing
their goals

OF course, the Extrovert Ideal is not a modern invention. Extroversion
is in our DNA—literally, according to some psychologists. The trait has
been found to be less prevalent in Asia and Africa than in Europe and
America, whose populations descend largely from the migrants of the
world. It makes sense, say these researchers, that world travelers were
more extroverted than those who stayed home—and that they passed on
their traits to their children and their children’s children. “

Toastmasters, the nonprofit organization established in 1924 whose
members meet weekly to practice public speaking and whose founder
declared that “all talking is selling and all selling involves talking,”
is still thriving, with more than 12,500 chapters in 113 countries.

Don Chen will graduate into a business culture in which verbal fluency
and sociability are the two most important predictors of success,
according to a Stanford Business School study

In one experiment in which two strangers met over the phone, those who
spoke more were considered more intelligent, better looking, and more
likable. We also see talkers as leaders. The more a person talks, the
more other group members direct their attention to him, which means that
he becomes increasingly powerful as a meeting goes on. It also helps to
speak fast; we rate quick talkers as more capable and appealing than
slow talkers.

It’s about a family sitting on a porch in Texas on a hot summer day, and
somebody says, ‘I’m bored. Why don’t we go to Abilene?’ When they get to
Abilene, somebody says, ‘You know, I didn’t really want to go.’ And the
next person says, ‘I didn’t want to go—I thought you wanted to go,’ and
so on. Whenever you’re in an army group and somebody says, ‘I think
we’re all getting on the bus to Abilene here,’ that is a red flag. You
can stop a conversation with it. It is a very powerful artifact of our
culture

Because of their inclination to listen to others and lack of interest in
dominating social situations, introverts are more likely to hear and
implement suggestions. Having benefited from the talents of their
followers, they are then likely to motivate them to be even more
proactive.

Guy Kawasaki, the best-selling author, speaker, serial entrepreneur, and
Silicon Valley legend, tweeted, “You may find this hard to believe, but
I am an introvert. I have a ‘role’ to play, but I fundamentally am a
loner.

Wouldn’t it be a great irony,” he asked, “if the leading proponents of
the ‘it’s about people’ mantra weren’t so enamored with meeting large
groups of people in real life? Perhaps social media affords us the
control we lack in real life socializing: the screen as a barrier
between us and the world.

Most inventors and engineers I’ve met are like me—they’re shy and they
live in their heads. They’re almost like artists. In fact, the very best
of them are artists. And artists work best alone where they can control
an invention’s design without a lot of other people designing it for
marketing or some other committee. I don’t believe anything really
revolutionary has been invented by committee. If you’re that rare
engineer who’s an inventor and also an artist, I’m going to give you
some advice that might be hard to take. That advice is: Work alone.
You’re going to be best able to design revolutionary products and
features if you’re working on your own. Not on a committee. Not on a
team.

On the Internet, wondrous creations were produced via shared brainpower:
Linux, the open-source operating system; Wikipedia, the online
encyclopedia; MoveOn.org, the grassroots political movement. These
collective productions, exponentially greater than the sum of their
parts, were so awe-inspiring that we came to revere the hive mind, the
wisdom of crowds, the miracle of crowdsourcing. Collaboration became a
sacred concept—the key multiplier for success.

Is a truism in tech that open source attracts introverts,” says Dave W.
Smith, a consultant and software developer in Silicon Valley, referring
to the practice of producing software by opening the source code to the
online public and allowing anyone to copy, improve upon, and distribute
it. Many of these people were motivated by a desire to contribute to the
broader good, and to see their achievements recognized by a community
they valued.

What’s so magical about solitude? In many fields, Ericsson told me, it’s
only when you’re alone that you can engage in Deliberate Practice, which
he has identified as the key to exceptional achievement. When you
practice deliberately, you identify the tasks or knowledge that are just
out of your reach, strive to upgrade your performance, monitor your
progress, and revise accordingly.

Another study, of 38,000 knowledge workers across different sectors,
found that the simple act of being interrupted is one of the biggest
barriers to productivity. Even multitasking, that prized feat of
modern-day office warriors, turns out to be a myth. Scientists now know
that the brain is incapable of paying attention to two things at the
same time

open-plan offices from many different industries corroborates the
results of the games. Open-plan offices have been found to reduce
productivity and impair memory

Since then, some forty years of research has reached the same startling
conclusion. Studies have shown that performance gets worse as group size
increases: groups of nine generate fewer and poorer ideas compared to
groups of six, which do worse than groups of four

What created Linux, or Wikipedia, if not a gigantic electronic
brainstorming session? But we’re so impressed by the power of online
collaboration that we’ve come to overvalue all group work at the expense
of solo thought. We fail to realize that participating in an online
working group is a form of solitude all its own

Psychologists usually offer three explanations for the failure of group
brainstorming. The first is social loafing: in a group, some individuals
tend to sit back and let others do the work. The second is production
blocking: only one person can talk or produce an idea at once, while the
other group members are forced to sit passively. And the third is
evaluation apprehension, meaning the fear of looking stupid in front of
one’s peers.

That was exactly what happened—the conformists showed less brain
activity in the frontal, decision-making regions and more in the areas
of the brain associated with perception. Peer pressure, in other words,
is not only unpleasant, but can actually change your view of a problem.

Indeed, studies show that face-to-face interactions create trust in a
way that online interactions can’t. Research also suggests that
population density is correlated with innovation; despite the advantages
of quiet walks in the woods, people in crowded cities benefit from the
web of interactions that urban life offers.

But the café worked as my office because it had specific attributes that
are absent from many modern schools and workplaces. It was social, yet
its casual, come-and-go-as-you-please nature left me free from unwelcome
entanglements and able to “deliberately practice” my writing. I could
toggle back and forth between observer and social actor as much as I
wanted. I could also control my environment

I'm suggesting, is not to stop collaborating face-to-face, but to refine
the way we do it. For one thing, we should actively seek out symbiotic
introvert-extrovert relationships, in which leadership and other tasks
are divided according to people’s natural strengths and temperaments.
The most effective teams are composed of a healthy mix of introverts and
extroverts, studies show, and so are many leadership structures.

Many of the children turned out exactly as Kagan had expected. The
high-reactive infants, the 20 percent who’d hollered at the mobiles
bobbing above their heads, were more likely to have developed serious,
careful personalities. The low-reactive infants—the quiet ones—were more
likely to have become relaxed and confident types. High and low
reactivity tended to correspond, in other words, to introversion and
extroversion.

Temperament refers to inborn, biologically based behavioral and
emotional patterns that are observable in infancy and early childhood;
personality is the complex brew that emerges after cultural influence
and personal experience are thrown into the mix. Some say that
temperament is the foundation, and personality is the building

One theory, based on the writings of the sociobiologist E. O. Wilson,
holds that when our ancestors lived on the savannah, being watched
intently meant only one thing: a wild animal was stalking us. And when
we think we’re about to be eaten, do we stand tall and hold forth
confidently? No. We run. In other words, hundreds of thousands of years
of evolution urge us to get the hell off the stage, where we can mistake
the gaze of the spectators for the glint in a predator’s eye

the orchid hypothesis” by David Dobbs in a wonderful article in The
Atlantic. This theory holds that many children are like dandelions, able
to thrive in just about any environment. But others, including the
high-reactive types that Kagan studied, are more like orchids: they wilt
easily, but under the right conditions can grow strong and magnificent.

these high-reactive monkeys owed their success to the enormous amounts
of time they spent watching rather than participating in the group,
absorbing on a deep level the laws of social dynamics.

a high-reactive child’s ideal parent: someone who “can read your cues
and respect your individuality; is warm and firm in placing demands on
you without being harsh or hostile; promotes curiosity, academic
achievement, delayed gratification, and self-control; and is not harsh,
neglectful, or inconsistent

Enjoyment appears at the boundary between boredom and anxiety, when the
challenges are just balanced with the person’s capacity to act. —MIHALY
CSIKSZENTMIHALYI

Bill Gates is never going to be Bill Clinton, no matter how he polishes
his social skills, and Bill Clinton can never be Bill Gates, no matter
how much time he spends alone with a computer. We might call this the
“rubber band theory” of personality. We are like rubber bands at rest.
We are elastic and can stretch ourselves, but only so much.

influential research psychologist named Hans Eysenck hypothesized that
human beings seek “just right” levels of stimulation—not too much and
not too little. Stimulation is the amount of input we have coming in
from the outside world. It can take any number of forms, from noise to
social life to flashing lights

In another famous study, introverts and extroverts were asked to play a
challenging word game in which they had to learn, through trial and
error, the governing principle of the game. While playing, they wore
headphones that emitted random bursts of noise. They were asked to
adjust the volume of their headsets up or down to the level that was
“just right.” On average, the extroverts chose a noise level of 72
decibels, while the introverts selected only 55 decibels

In one well-known experiment, dating all the way back to 1967 and still
a favorite in-class demonstration in psychology courses, Eysenck placed
lemon juice on the tongues of adult introverts and extroverts to find
out who salivated more. Sure enough, the introverts, being more aroused
by sensory stimuli, were the ones with the watery mouths.

Imagine how much better you’ll be at this sweet-spot game once you’re
aware of playing it. You can set up your work, your hobbies, and your
social life so that you spend as much time inside your sweet spot as
possible. People who are aware of their sweet spots have the power to
leave jobs that exhaust them and start new and satisfying businesses.

Esther could live to be a one-hundred-year-old lawyer, in other words,
the most knowledgeable practitioner in her field, and she might never be
comfortable speaking extemporaneously. She might find herself
perpetually unable, at speech time, to draw on the massive body of data
sitting inside her long-term memory.

may also help explain why they’re so bored by small talk. “If you’re
thinking in more complicated ways,” she told me, “then talking about the
weather or where you went for the holidays is not quite as interesting
as talking about values or morality.”

people with the gene variant of 5-HTTLPR that characterized the rhesus
monkeys of chapter 3) inside

This is an especially important set of attributes at a time when a 2010
University of Michigan study shows that college students today are 40
percent less empathetic than they were thirty years ago, with much of
the drop having occurred since 2000. (The study’s authors speculate that
the decline in empathy is related to the prevalence of social media,
reality TV, and “hyper-competitiveness.”)

he had to choose his mate by asking a single question at a speed-dating
event, the question he would choose is: “What was your last embarrassing
experience?” Then he would watch very carefully for lip-presses,
blushing, and averted eyes. “The elements of the embarrassment are
fleeting statements the individual makes about his or her respect for
the judgment of others,” he writes. “Embarrassment reveals how much the
individual cares about the rules that bind us to one another.”

From fruit flies to house cats to mountain goats, from sunfish to
bushbaby primates to Eurasian tit birds, scientists have discovered that
approximately 20 percent of the members of many species are “slow to
warm up,” while the other 80 percent are “fast” types who venture forth
boldly without noticing much of what’s going on around them.
(Intriguingly, the percentage of infants in Kagan’s lab who were born
high-reactive was also, you’ll recall, about twenty.)

about Kagan’s research, and Aron’s, he might have been less surprised by
his colleagues’ reactions. He might even have used his insight into
personality psychology to get them to listen. Congress, he could have
safely assumed, is made up of some of the least sensitive people in the
country—people who, if they’d been kids in one of Kagan’s experiments,
would have marched up to oddly attired clowns and strange ladies wearing
gas masks without so much as a backward glance at their mothers

That’s because this mission, for him, is not about politics or
personality. It’s about the call of his conscience. “It’s about the
survival of the planet,” he says. “Nobody is going to care who won or
lost any election when the earth is uninhabitable.”

In most settings, people use small talk as a way of relaxing into a new
relationship, and only once they’re comfortable do they connect more
seriously. Sensitive people seem to do the reverse. They “enjoy small
talk only after they’ve gone deep,” says Strickland. “When sensitive
people are in environments that nurture their authenticity, they laugh
and chitchat just as much as anyone else.”

The answer is yes, except that some of us do so more than others. Dorn
has observed that her extroverted clients are more likely to be highly
reward-sensitive, while the introverts are more likely to pay attention
to warning signals. They’re more successful at regulating their feelings
of desire or excitement. They protect themselves better from the
downside

In fact, some scientists are starting to explore the idea that
reward-sensitivity is not only an interesting feature of extroversion;
it is what makes an extrovert an extrovert. Extroverts, in other words,
are characterized by their tendency to seek rewards, from top dog status
to sexual highs to cold cash. They’ve been found to have greater
economic, political, and hedonistic ambitions than introverts; even
their sociability is a function of reward-sensitivity, according to this
view—extroverts socialize because human connection is inherently
gratifying.

Extroverts’ dopamine pathways appear to be more active than those of
introverts. Although the exact relationship between extroversion,
dopamine, and the brain’s reward system has not been conclusively
established, early findings have been intriguing

Introverts also seem to be better than extroverts at delaying
gratification, a crucial life skill associated with everything from
higher SAT scores and income to lower body mass index. In one study,
scientists gave participants the choice of a small reward immediately (a
gift certificate from Amazon) or a bigger gift certificate in two to
four weeks. Objectively, the bigger reward in the near but not immediate
future was the more desirable option. But many people went for the “I
want it now” choice—and when they did, a brain scanner revealed that
their reward network was activated. Those who held out for the larger
reward two weeks hence showed more activity in the prefrontal cortex

with certain personality types got control of capital and institutions
and power,” Curry told me. “And people who are congenitally more
cautious and introverted and statistical in their thinking became
discredited and pushed aside

There have been some complaints, Vince, that you’re not helping people
to do transactions,” the president of Enron told him, according to
Conspiracy of Fools, a book about the Enron scandal. “Instead, you’re
spending all your time acting like cops. We don’t need cops, Vince.”

In many East Asian classrooms, the traditional curriculum emphasizes
listening, writing, reading, and memorization. Talking is simply not a
focus, and is even discouraged

So it’s telling that even Hung recalls her culture shock upon entering
her first American-style classroom. She considered it rude to
participate in class because she didn’t want to waste her classmates’
time

Others felt that Asian students shouldn’t be forced to speak up and
conform to the Western mode. “Perhaps instead of trying to change their
ways, colleges can learn to listen to their sound of silence,” wrote
Heejung Kim, a Stanford University cultural psychologist, in a paper
arguing that talking is not always a positive act.

Those who know do not speak.Those who speak do not know.—LAO ZI, The Way
of Lao Zi

These results would not surprise anyone familiar with traditional Asian
attitudes to the spoken word: talk is for communicating need-to-know
information; quiet and introspection are signs of deep thought and
higher truth. Words are potentially dangerous weapons that reveal things
better left unsaid.

Speech is civilization itself. The word, even the most contradictory
word, preserves contact—it is silence which isolates.—THOMAS MANN, The
Magic Mountain

And it’s because of relationship-honoring that Hiroshima victims
apologized to each other for surviving. “Their civility has been well
documented but still stays the heart,” writes the essayist Lydia Millet

Mike sounded dismissive of Western communication styles, but he admitted
that he sometimes wished he could be noisy and uninhibited himself.
“They’re more comfortable with their own character,” he said of his
Caucasian classmates. Asians are “not uncomfortable with who they are,
but are uncomfortable with expressing who they are

The other students seemed to have no problem joking around and asking
intelligent questions. “Mike, you were so loud today,” the professor
teased him when finally he said good-bye. “You just blew me away.” He
left her house feeling bad about himself. “People who don’t talk are
seen as weak or lacking,” he concluded ruefully.

It may not be fair, and it might not be the best way of judging a
person’s contribution to the bottom line, “but if you don’t have
charisma you can be the most brilliant person in the world and you’ll
still be disrespected.”

Remember, in Silicon Valley, you can be the smartest, most capable
person, but if you can’t express yourself aside from showing your work,
you’ll be underappreciated. Many foreign-born professionals experience
this; you’re a glorified laborer instead of a leader.”

In Asian cultures,” Ni said, “there’s often a subtle way to get what you
want. It’s not always aggressive, but it can be very determined and very
skillful. In the end, much is achieved because of it. Aggressive power
beats you up; soft power wins you over.”

The day arrived when he stood to take the oath, at which point the chief
justice ordered him to take off his turban. Gandhi saw his true
limitations then. He knew that resistance would be justified, but
believed in picking his battles, so he took off his headgear. His
friends were upset. They said he was weak, that he should have stood up
for his beliefs. But Gandhi felt that he had learned “to appreciate the
beauty of compromise

But as the word satyagraha implies, Gandhi’s passivity was not weakness
at all. It meant focusing on an ultimate goal and refusing to divert
energy to unnecessary skirmishes along the way. Restraint, Gandhi
believed, was one of his greatest assets

I have naturally formed the habit of restraining my thoughts. A
thoughtless word hardly ever escaped my tongue or pen. Experience has
taught me that silence is part of the spiritual discipline of a votary
of truth. We find so many people impatient to talk. All this talking can
hardly be said to be of any benefit to the world. It is so much waste of
time

For example, the cross-cultural psychologist Priscilla Blinco gave
Japanese and American first graders an unsolvable puzzle to work on in
solitude, without the help of other children or a teacher, and compared
how long they tried before giving up. The Japanese children spent an
average of 13.93 minutes on the puzzle before calling it quits, whereas
the American kids spent only 9.47 minutes. Fewer than 27 percent of the
American students persisted as long as the average Japanese student—and
only 10 percent of the Japanese students gave up as quickly as the
average American. Blinco attributes these results to the Japanese
quality of persistence.

People who are “rejection-sensitive” are warm and loving when they feel
secure, hostile and controlling when they feel rejected

The answer, he says, is simple, and it has to do with a new field of
psychology that he created almost singlehandedly, called Free Trait
Theory. Little believes that fixed traits and free traits coexist.
According to Free Trait Theory, we are born and culturally endowed with
certain personality traits—introversion, for example—but we can and do
act out of character in the service of “core personal projects.”

The genius of Little’s theory is how neatly it resolves this discomfort.
Yes, we are only pretending to be extroverts, and yes, such
inauthenticity can be morally ambiguous (not to mention exhausting), but
if it’s in the service of love or a professional calling, then we’re
doing just as Shakespeare advised

Then Lippa did the same thing with actual extroverts and compared the
results. He found that although the latter group came across as more
extroverted, some of the pseudo-extroverts were surprisingly convincing.
It seems that most of us know how to fake it to some extent

When Professor Little introduced the concept of self-monitoring to his
personality psychology classes, some students got very worked up about
whether it was ethical to be a high self-monitor.

But Little, an ethical and sympathetic man who happens to be an
extremely high self-monitor, sees things differently. He views
self-monitoring as an act of modesty. It’s about accommodating oneself
to situational norms, rather than “grinding down everything to one’s own
needs and concerns.”

Restorative niche” is Professor Little’s term for the place you go when
you want to return to your true self. It can be a physical place, like
the path beside the Richelieu River, or a temporal one, like the quiet
breaks you plan between sales calls. It can mean canceling your social
plans on the weekend before a big meeting at work

Or perhaps you’ve always dreamed of building your own small company,
working from home so you can spend more time with your spouse and
children. You know you’ll need to do a certain amount of networking, so
you make the following Free Trait Agreement with yourself: you will go
to one schmooze-fest per week. At each event you will have at least one
genuine conversation (since this comes easier to you than “working the
room”) and follow up with that person the next day. After that, you get
to go home and not feel bad when you turn down other networking
opportunities that come your way.

People who tend to [suppress their negative emotions] regularly,”
concludes Grob, “might start to see the world in a more negative light

They focused on the so-called Big Five traits:
Introversion-Extroversion; Agreeableness; Openness to Experience;
Conscientiousness; and Emotional Stability. (Many personality
psychologists believe that human personality can be boiled down to these
five characteristics.)

We might say the same of Greg and Emily. When Emily lowers her voice and
flattens her affect during fights with Greg, she thinks she’s being
respectful by taking the trouble not to let her negative emotions show.
But Greg thinks she’s checking out or, worse, that she doesn’t give a
damn. Similarly, when Greg lets his anger fly, he assumes that Emily
feels, as he does, that this is a healthy and honest expression of their
deeply committed relationship. But to Emily, it’s as if Greg has
suddenly turned on her.

We’re best off when we don’t allow ourselves to go to our angry place.
Amazingly, neuroscientists have even found that people who use Botox,
which prevents them from making angry faces, seem to be less anger-prone
than those who don’t, because the very act of frowning triggers the
amygdala to process negative emotions

But in sales there’s a truism that ‘we have two ears and one mouth and
we should use them proportionately.’ I believe that’s what makes someone
really good at selling or consulting—the number-one thing is they’ve got
to really listen well.

The key is to expose your child gradually to new situations and
people—taking care to respect his limits, even when they seem extreme.
This produces more-confident kids than either overprotection or pushing
too hard

One of the best things you can do for an introverted child is to work
with him on his reaction to novelty. Remember that introverts react not
only to new people, but also to new places and events. So don’t mistake
your child’s caution in new situations for an inability to relate to
others. He’s recoiling from novelty or overstimulation, not from human
contact

you can, it’s best to teach your child self-coaxing skills while he’s
still very young, when there’s less stigma associated with social
hesitancy. Be a role model by greeting strangers in a calm and friendly
way, and by getting together with your own friends. Similarly, invite
some of his classmates to your house.

If you think that you’re not up to all this, or that your child could
use extra practice, ask a pediatrician for help locating a social skills
workshop in your area. These workshops teach kids how to enter groups,
introduce themselves to new peers, and read body language and facial
expressions. And they can help your child navigate what for many
introverted kids is the trickiest part of their social lives: the school
day.

You can also teach your child simple social strategies to get him
through uncomfortable moments. Encourage him to look confident even if
he’s not feeling it. Three simple reminders go a long way: smile, stand
up straight, and make eye contact. Teach him to look for friendly faces
in a crowd

The truth is that many schools are designed for extroverts. Introverts
need different kinds of instruction from extroverts, write College of
William and Mary education scholars Jill Burruss and Lisa Kaenzig. And
too often, “very little is made available to that learner except
constant advice on becoming more social and gregarious.”

As adults, they get to select the careers, spouses, and social circles
that suit them. They don’t have to live in whatever culture they’re
plunked into. Research from a field known as “person-environment fit”
shows that people flourish when, in the words of psychologist Brian
Little, they’re “engaged in occupations, roles or settings that are
concordant with their personalities.” The inverse is also true: kids
stop learning when they feel emotionally threatened.

Researchers have found that intense engagement in and commitment to an
activity is a proven route to happiness and well-being. Well-developed
talents and interests can be a great source of confidence for your
child, no matter how different he might feel from his peers.

The secret to life is to put yourself in the right lighting. For some
it’s a Broadway spotlight; for others, a lamplit desk. Use your natural
powers—of persistence, concentration, insight, and sensitivity—to do
work you love and work that matters. Solve problems, make art, think
deeply.

Also, remember the dangers of the New Groupthink. If it’s creativity
you’re after, ask your employees to solve problems alone before sharing
their ideas. If you want the wisdom of the crowd, gather it
electronically, or in writing, and make sure people can’t see each
other’s ideas until everyone’s had a chance to contribute. Face-to-face
contact is important because it builds trust, but group dynamics contain
unavoidable impediments to creative thinking.
