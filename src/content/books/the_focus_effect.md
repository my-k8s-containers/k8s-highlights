---
type: books
weight: 7
title: The Focus Effect
subtitle: Change Your Work, Change Your Life
author: Greg Wells
thumbnail: images/the_focus_effect.jpg
---

Many parents use iPads and other digital devices to pacify and occupy their young children. These parents are inadvertently creating an atmosphere where children are no longer engaging in conversations or storytelling.

The average person spends more than sixty hours a year just looking to see if they have a text or email, not actually reading it.

Today, there are more mobile phones in the world than there are toilets.

Annually, in the world of disputes, 60 million disagreements among eBay traders are resolved using ‘online dispute resolution’ rather than lawyers and judges—this is three times the number of lawsuits filed each year in the entire U.S. court system.

Most children today take in more information in one week than their great-great-grandparents did in their entire lifetimes.

She came over to me and used a line from her favorite movie, Shrek, to call out my bad behavior. “Daddy,” she said, “your job is not my problem,” quoting Princess Fiona when she’s just about had it with her ogre companion.

They now know the boundaries. When I’m with them, I’m with them. When I’m working, I’m working. My personal life and my work life are no longer all meshed together.

I put my eyes on the boots of the person in front of me, and focused on nothing else for the next few hours until we reached the bottom.

Companies must begin to develop cultures where distraction is no longer draining energy and stealing productivity.

When we focus on others, we lose the ability to perform and reach our potential because we start judging.

Most people live within the confines of their comfort zone. The best thing you can do for yourself is to regularly move beyond it. This is the way to lasting personal mastery and to realize the true potential of your human endowments. —Robin Sharma

If we task-switch and jump back and forth between activities, different parts of the brain are activated. As a result, blood flow has to move from one location to another, and that takes time.

Studies also show that in terms of productive time during the day, you have only about 90 minutes to 120 minutes maximum productive time. It does not matter who you are. The question is, what are you doing with this time?

We’ve all been to lunch with that coworker who has a cheeseburger, french fries, and a glass of wine, then goes back to work. How much quality work can he really accomplish? Yet we live in a society obsessed with food

A state of flow has four qualities: (1) no distractions, (2) a level of activation that is neither too low (boredom or apathy) nor too high (stress and anxiousness), (3) a challenge that is slightly beyond your comfort zone, and (4) deliberate control of emotions to enable complete immersion in the task.

The concept of the eight-hour workday started with the Industrial Revolution. Ford Motor Company advanced the idea in 1914, when the company scaled back its forty-eight-hour work week to curb accidents.

...companies, and even my own company, are going to have to reengineer our workplace environment, structure, and experience if we want to be around in twenty years. Given the choice, why would a millennial choose long, unhealthy work weeks...

When a working environment is shaped by an emphasis on the value of employees’ time at work rather than a fixed notion of how long they work, good things happen.

The average North American today never reads a book past age forty. It’s not because they’re not interested—they simply can’t stay focused on anything that demands more of their attention than the average social media post.

Working together to monitor the overall effectiveness of everything from meetings to email communications or individual work can enable a widespread improvement in overall effectiveness and a simultaneous improvement in joy and wellness.

Most athletes stopped competing in their late teens or early twenties. This went on until the early 1990s, when a cultural shift happened in coaching that completely revolutionized the field. Athletes still trained at a very high level, but training was interspersed with more recovery time. They started incorporating nutrition, massage, and sleep into their routines to help them recover and regenerate faster. 

The old cliché “The early bird gets the worm” is no longer true. It’s been replaced with “The second mouse gets the cheese.”

Allowing employees to work fewer hours shows you value the lifestyle-workplace balance. It sends the message of, Yes, we expect you to work well, and we’re going to teach you how to be more productive, and we’re going to provide you with the right tools to be focused.

Allocate two or three times a day when you will read and respond to emails and let everyone in your orbit know that they can expect to hear from you at those times.

In order to have an impact in the world, which is what I’m trying to do, you cannot constantly react to people’s instantaneous communications. It costs too much time to move from task to task to task.

Leaders can also serve as an example by working reasonable hours. If the employer is the one coming in working sixty to seventy hours a week, no employee is ever going to feel OK cutting back their own hours.

Tell them, “If you’re trying to impress me, you’re not doing it. I will be more impressed when you manage your work to the point where you are able to go home.”

Offer employees an opportunity to work shorter hours if they are more productive during the time they are at work, and the results will shock you. The quality of work will increase, performance will improve, and everyone will enjoy working.

Meetings are one of the biggest time wasters in any organization because there are so many ways they can become unproductive.

There is no passion to be found in playing small—in settling for a life that is less than the one you are capable of living. —Nelson Mandela.

Our children are not learning the art of conversation; they are learning from their parents that it is OK to be disengaged and not present. I worry about what this will look like for these children when they are adults.

Corporations are infamous for bogging down in a range of urgent or seemingly important issues that aren’t actually adding value. By helping your teams shift from time management (living by the calendar) to priority management (doing the most important things well), you can create a wave of focus and wellness.

Although we are speaking about a new way of working, it’s really about setting the stage for the future and putting people in a position where they can respond instead of reacting.

“I’m constantly in the pursuit of perfection.”


