---
type: books
weight: 7
title: Remote
subtitle: Office Not Required
author: Jason Fried
thumbnail: images/remote.jpg
---

Remote shows you how to remove the final barrier to doing the work you
were meant to do, with the people you were meant to do it with, in the
most rewarding and profitable way possible—this book is your ticket to
real freedom

If you ask people where they go when they really need to get work done,
very few will respond “the office.” If they do say the office, they’ll
include a qualifier such as “super early in the morning before anyone
gets in” or “I stay late at night after everyone’s left” or “I sneak in
on the weekend.”

That’s 1.5 hours a day, 7.5 hours per week, or somewhere between 300 and
hours is exactly the amount of programmer time we spent building
Basecamp, our most popular product. Imagine what you could do with 400
extra hours a year. Commuting isn’t just bad for you, your
relationships, and the environment—it’s bad for business

The big transition with a distributed workforce is going from
synchronous to asynchronous collaboration. Not only do we not have to be
in the same spot to work together, we also don’t have to work at the
same time to work together.

Thankfully, the population-density benefits that suited factories proved
great for lots of other things too. We got libraries, stadiums,
theaters, restaurants, and all the other wonders of modern culture and
civilization. But we also got cubicles, tiny apartments, and sardine
boxes to take us from here to there. We traded the freedom and splendor
of country land and fresh air for convenience and excitement.

The new luxury is the luxury of freedom and time. Once you’ve had a
taste of that life, no corner office or fancy chef will be able to drag
you back.

into. At first, giving up seeing your coworkers in person every day
might come as a relief (if you’re an introvert), but eventually you’re
likely to feel a loss. Even with the substitutes we’ll discuss, there
are times when nothing beats talking to your manager in person or
sitting in a room with your colleagues, brainstorming the next big thing

That’s just it—if you can’t let your employees work from home out of
fear they’ll slack off without your supervision, you’re a babysitter,
not a manager. Remote work is very likely the least of your problems

As Chris Hoffman from the IT Collective explains: “If we’re struggling
with trust issues, it means we made a poor hiring decision. If a team
member isn’t producing good results or can’t manage their own schedule
and workload, we aren’t going to continue to work with that person. It’s
as simple as that. We employ team members who are skilled professionals,
capable of managing their own schedules and making a valuable
contribution to the organization. We have no desire to be babysitters
during the day

People have an amazing ability to live down to low expectations. If you
run your ship with the conviction that everyone’s a slacker, your
employees will put all their ingenuity into proving you right. If you
view those who work under you as capable adults who will push themselves
to excel even when you’re not breathing down their necks, they’ll
delight you in return

The bottom line is that you shouldn’t hire people you don’t trust, or
work for bosses who don’t trust you. If you’re not trusted to work
remotely, why are you trusted to do anything at all

You certainly don’t need everyone physically together to create a strong
culture. The best cultures derive from actions people actually take, not
the ones they write about in a mission statement. Newcomers to an
organization arrive with their eyes open. They see how decisions are
made, the care that’s taken, the way problems are fixed, and so forth.

To a lot of people, being the big boss is about achieving such control.
It’s woven into their identity. To such alpha males and females, having
someone under “direct supervision” means having them in their line of
sight—literally. The thinking goes, If I can see them, I can control
them

At 37signals we’ve institutionalized this through a weekly discussion
thread with the subject “What have you been working on?” Everyone chimes
in with a few lines about what they’ve done over the past week and
what’s intended for the next week. It’s not a precise, rigorous
estimation process, and it doesn’t attempt to deal with coordination. It
simply aims to make everyone feel like they’re in the same galley and
not their own little rowboat.

One of the secret benefits of hiring remote workers is that the work
itself becomes the yardstick to judge someone’s performance. When you
can’t see someone all day long, the only thing you have to evaluate is
the work

Additionally, AFA employees who do not otherwise work remotely are asked
to do so at least once or twice per month so they’ll be ready if they
have to during a disaster. The company also encourages everyone to stay
home during the peak of flu season or during scares like H1N1.

We believe that these staples of work life—meetings and managers—are
actually the greatest causes of work not getting done at the office.
That, in fact, the further away you are from meetings and managers, the
more work gets done. This is one of the key reasons we’re so
enthusiastic about remote work!

Constantly asking people what they’re working on prevents them from
actually doing the work they’re describing

Meetings should be like salt—sprinkled carefully to enhance a dish, not
poured recklessly over every forkful. Too much salt destroys a dish. Too
many meetings can destroy morale and motivation.

There are no hero awards for putting in more than that. Sure, every now
and then there’s the need for a short sprint, but, most of the time, the
company is viewing what it does as a marathon. It’s crucial for everyone
to pace themselves.

but purposely hires remote workers in other parts of the world because
he feels having an international team helps him win clients. Having
input from Texas, London, and Auckland, New Zealand, contributes to a
wider range of ideas and perspectives.

Remember, doing great work with great people is one of the most durable
sources of happiness we humans can tap into. Stick with it.

This gives back the edge to quiet-but-productive workers who often lose
out in a traditional office environment

Remote work speeds up the process of getting the wrong people off the
bus and the right people on board.†

Would-be remote workers and managers have a lot to learn from how the
open source software movement has conquered the commercial giants over
the past decades. It’s a triumph of asynchronous collaboration and
communication like few the world has ever seen

When New York City’s subway system was plagued by crime and vandalism in
the 1990s, New York’s Police Commissioner William Bratton forced his
commanders to use the subway. When they saw with their own eyes how bad
things were, change soon followed.

Morale and motivation are fragile things, so you want to make sure to
monitor the pulse of your remote workforce

What is the case is that people are often scared to make a decision
because they work in an environment of retribution and blame. That style
of work is very incompatible with remote work. As a manager, you have to
accept the fact that people will make mistakes, but not intentionally,
and that mistakes are the price of learning and self-sufficiency.

Motivation is the fuel of intellectual work. You can get several days’
worth of work completed in one motivation-turboed afternoon. Or, when
you’re motivation starved, you can waste a week getting a day’s worth of
work done.

Rather, the only reliable way to muster motivation is by encouraging
people to work on the stuff they like and care about, with people they
like and care about. There are no shortcuts.

When I retire, I’m going to travel the world” is a common dream, but why
wait for retirement? If seeing the world is your passion, you shouldn’t
wait until old age to pursue it. And if you’re working remotely, you
can’t use the “but I have a job” excuse to defer living.

Remote work has already progressed through the first two stages of
Gandhi’s model for change: “First they ignore you, then they laugh at
you, then they fight you, then you win.” We are squarely in the fighting
stage—the toughest one—but it’s also the last one before you win.

Remote work is here, and it’s here to stay. The only question is whether
you’ll be part of the early adopters, the early majority, the late
majority, or the laggards.‡ The ship carrying the innovators has already
sailed, but there are still plenty of vessels for the early adopters.
Come on board.

